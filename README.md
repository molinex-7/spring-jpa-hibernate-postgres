# Spring JPA Hibernate...

Made by Thiago Molina with technologies like Spring, JPA, Hibernate, Postgres,
Maven, Vim, curl, openJDK, Bash, Arch Linux...

Yeah, I'm tripping...

### Install

First of all install maven

    $ sudo pacman -S maven
	
After install postgres 

    $ sudo pacman -S postgresql

Configure and initialize postgres, clone this repo, make adjustments to the file

    application.properties

And run it, from the project root directory (the one with the pom.xml) 

    $ mvn spring-boot:run

### Tip

One time at http://start.spring.io

There they create archetypes for spring designs. If you want to create 
one from scratch...

### Contact

For any clarification contact us

    Mail: t.molinex@gmail.com

